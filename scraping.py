import os
import pandas as pd
import selenium
from selenium import webdriver
import time
from selenium.common.exceptions import StaleElementReferenceException, NoSuchElementException
import requests
import time

def csv_to_list(file):
    df = pd.read_csv(file)
    df = df['Nombre']
    print(df)
    return df


nombre_producto = csv_to_list('ProductosBodeguita.csv')
if not os.path.exists('imagenes'):
    os.makedirs('imagenes')
#url de la pagina que utilizaras
url = "https://www.google.com/search?q="
#fragmento para indicar que en google utilizaras el apartado de imagenes
url2 = "+&hl=es&source=lnms&tbm=isch"
#ruta donde se encuentra tu chromedrive, es necesario descargarlo
driver = webdriver.Chrome(r'/home/gerardo/chromedriver_linux64.exe')

#impresion para ver los elementos del dataframe
print(nombre_producto)
contador = 0
errores = []
nombre_actual = ""

#iteracion de los productos
for i in nombre_producto:
    try:
        #limpieza del texto
        i = i.replace(".", " ")
        i = i.replace("/", " ")
        search = i
        #aqui se puede modificar la busqueda 
        driver.get(url+search+ ' 300 x 300 ' +url2)
        nombre_actual = search
        #path de donde hara click el bot para ver la imagen y descargarla
        btn = driver.find_element("xpath", "//div[1]/div[1]/div/a/div/img").click()
        time.sleep(2)
        images_saree = []
        #path de la imagen que descargara
        elements = driver.find_elements("xpath", "//*[@id='Sva75c']/div[2]/div/div[2]/div[2]/div[2]/c-wiz/div/div[1]/div[2]/div[2]/div/a/img")
        for i in elements:
            image = i.get_attribute('src')
            images_saree.append(image)

        #aqui se va haciendo un request para descargar 
        for img in images_saree:
            file_name = search + '.jpg'
            r = requests.get(img, stream=True)
            time.sleep(1)
            if r.status_code == 200:
                #se guarda en la carpeta que tu desees
                with open(f"imagenes/" + file_name, 'wb') as f:
                    for chunk in r:
                        f.write(chunk)          
            else:
                print('Error ' + nombre_actual + str(r.status_code))
            break
        contador += 1
        #impresion para visualizar las imagenes que se van descargando
        print("Imagenes descargadas: ", contador, " de ", len(nombre_producto), " - ", search)
    except:
        print("Error en: ", nombre_actual)
        errores.append(nombre_actual)
        pass

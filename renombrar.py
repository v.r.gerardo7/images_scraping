import os
import pandas as pd

#ruta de imagenes ya descargadas
contenido = os.listdir("imagenes")
#ruta de tu csv de productos
#el uso de la informacion es mediante un dataframe
df = pd.read_csv('ProductosBodeguita.csv')
lista_nombres = []
lista_SKU = []

for i in range(len(df)-1):
    #limpieza para un nombre de producto legible
    df["Nombre"] = df["Nombre"].replace(".", " ")
    df["Nombre"] = df["Nombre"].replace("/", " ")
    
    lista_nombres.append(df["Nombre"])

for i in range(len(df)-1):
    #lista con todos los SKU de tus productos
    lista_SKU.append(df["SKU"])

#se renombra comparando el nombre de la imagne con la lista y quedando solo el SKU
for x in range(len(contenido)-1):
    for i in range(len(lista_nombres)-1):
        if contenido[x] == lista_nombres[i] + ".jpg": 
            os.rename(f'imagenes/{contenido[x]}', f'sku/{lista_SKU[i]}.jpg')
        else : print('no esta')
            
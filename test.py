import pandas as pd
import selenium
import os
from selenium import webdriver
import time
from selenium.common.exceptions import StaleElementReferenceException, NoSuchElementException
import requests
import time

#test de mercado libre

#ruta de tu chromedriver
driver = webdriver.Chrome(r'/home/gerardo/chromedriver_linux64.exe')
def csv_to_list(file):
    df = pd.read_csv(file)
    df = df["Nombre"]
    return df

products  = csv_to_list('faltantes.csv')

if not os.path.exists('imagenes'):
    os.makedirs('imagenes')


for name in products:

    url = "https://www.mercadolibre.com.mx/"
    driver.get(url)
    search = name
    input = driver.find_element("xpath", "/html/body/header/div/form/input")
    input.send_keys(search)
    btn = driver.find_element("xpath", "/html/body/header/div/form/button/div").click()
    time.sleep(1)
    images_saree = []
    elements = driver.find_elements("xpath", "//ol[1]/li/div/div/div/a/div/div/div/div/div/img")


    for i in elements:
        image = i.get_attribute('src')
        images_saree.append(image)

    for img in images_saree:
        file_name = search + " " + '.jpg'
        print(f"this is the file name:{file_name}")
        r = requests.get(img, stream=True)
        if r.status_code == 200:
            with open(f"imagenes/" + file_name, 'wb') as f:
                for chunk in r:
                    f.write(chunk)
        else:
            print('')
        break
    time.sleep(1)